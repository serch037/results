
Using 399199 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201511291009

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.0		 100	        100
Video	   0.0 +/- 0.0        0.0		 100	        100
X	   0.0 +/- 0.0        0.1		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.0 +/- 0.0        0.8		 100	        100
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.0 +/- 0.2        4.0		 100	        100
Memload	   0.0 +/- 0.0        0.0		 100	        100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.1		 100	        100
X	   0.0 +/- 0.4       16.7		 100	       99.9
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.0 +/- 0.0        1.1		 100	        100
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.0 +/- 0.0        0.2		 100	        100
Memload	   0.0 +/- 0.0        0.1		 100	        100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.0		 100	        100
Video	   0.0 +/- 0.0        0.1		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.0 +/- 0.0        0.0		 100	        100
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.0 +/- 0.0        0.0		 100	        100
Memload	   0.0 +/- 0.0        0.0		 100	        100

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU
None	   0.0 +/- 0.0        0.0		 100
Video	   0.0 +/- 0.0        0.0		 100
X	   0.0 +/- 0.0        0.0		 100
Burn	   0.0 +/- 0.0        0.0		 100
Write	   0.0 +/- 0.0        0.0		 100
Read	   0.0 +/- 0.0        0.0		 100
Compile	   0.0 +/- 0.0        0.0		 100
Memload	   0.0 +/- 0.0        0.0		 100


