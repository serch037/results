
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201601131204

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.0		 100	        100
Video	   0.0 +/- 0.0        0.0		 100	        100
X	   0.0 +/- 0.0        0.1		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.0 +/- 0.0        0.1		 100	        100
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.0 +/- 0.0        0.1		 100	        100
Memload	   0.0 +/- 0.0        0.0		 100	        100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.1		 100	        100
X	   0.1 +/- 1.4       16.7		 100	       99.3
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.1 +/- 1.0       17.0		 100	       99.7
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.1 +/- 1.0       16.7		 100	       99.6
Memload	   0.1 +/- 0.8       16.7		 100	       99.8

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.4 +/- 1.9       12.0		90.6	         87
Video	   6.1 +/- 13.3      48.0		  56	       45.6
Burn	  12.1 +/- 24.2      85.0		36.3	       27.7
Write	   1.2 +/- 4.2       28.0		82.3	       77.5
Read	   1.1 +/- 3.5       15.0		82.3	       76.6
Compile	  15.8 +/- 28.9      84.0		33.8	       24.3
Memload	   0.7 +/- 2.4       11.0		85.9	       81.7

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU
None	   9.2 +/- 9.3       13.6		91.6
Video	  29.4 +/- 30.4      72.5		77.3
X	  10.9 +/- 11.3      25.6		90.2
Burn	  64.1 +/- 65.2      85.4		  61
Write	  11.9 +/- 12.5      32.2		89.4
Read	  11.2 +/- 11.3      12.1		89.9
Compile	  74.6 +/- 75.4     133.9		57.3
Memload	  10.1 +/- 10.5      36.4		90.9


