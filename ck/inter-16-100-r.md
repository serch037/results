
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201601131313

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	  10.0 +/- 11.1      26.0		 100	        100
Video	   8.0 +/- 8.8       15.0		 100	        100
X	   9.0 +/- 10.5      23.0		 100	        100
Burn	  10.0 +/- 10.6      23.0		 100	        100
Write	  20.0 +/- 21.4      37.0		 100	        100
Read	  22.0 +/- 22.7      80.0		 100	        100
Compile	  19.0 +/- 20.1      45.0		 100	        100
Memload	  18.0 +/- 19.7      38.0		 100	        100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	   8.0 +/- 8.9       25.0		 100	        100
X	   8.0 +/- 8.6       20.0		 100	        100
Burn	   7.0 +/- 8.5       21.0		 100	        100
Write	  13.0 +/- 14.9      38.0		 100	        100
Read	  16.0 +/- 17.2      53.0		 100	        100
Compile	  14.0 +/- 15.3      41.0		 100	        100
Memload	  16.0 +/- 18.0      36.0		 100	        100


