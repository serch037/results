
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201601131254

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	  24.0 +/- 24.4      28.0		 100	        100
Video	  20.0 +/- 20.5      30.0		 100	        100
X	  20.0 +/- 21.2      56.0		 100	        100
Burn	   8.0 +/- 8.2       12.0		 100	        100
Write	  27.0 +/- 27.7      49.0		 100	        100
Read	  32.0 +/- 33.7      87.0		 100	        100
Compile	  18.0 +/- 18.6      37.0		 100	        100
Memload	  36.0 +/- 36.8      56.0		 100	        100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	 392.0 +/- 2483.6  16701.0		 100	       97.8
X	 130.0 +/- 1367.2  16690.0		 100	       99.3
Burn	   6.0 +/- 6.7       15.0		 100	        100
Write	1172.0 +/- 4381.9  16766.0		 100	       93.1
Read	 820.0 +/- 3648.6  16734.0		 100	       95.2
Compile	  13.0 +/- 14.3      33.0		 100	        100
Memload	  59.0 +/- 683.0   16708.0		 100	       99.8


