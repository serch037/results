run #,run date,hostname,physical cores/threads,kernel,time(sec),benchmark
1,2015-09-13 15:31:36,evoarch,2,4.1.6-1-ck,199.130427256,compress
2,2015-09-13 15:34:55,evoarch,2,4.1.6-1-ck,201.332428260,compress
3,2015-09-13 15:38:16,evoarch,2,4.1.6-1-ck,216.223099622,compress
4,2015-09-13 15:41:52,evoarch,2,4.1.6-1-ck,197.438932051,compress
5,2015-09-13 15:45:10,evoarch,2,4.1.6-1-ck,212.564974159,compress
6,2015-09-13 15:48:43,evoarch,2,4.1.6-1-ck,203.169436474,compress
7,2015-09-13 15:52:06,evoarch,2,4.1.6-1-ck,208.263793163,compress
8,2015-09-13 15:55:34,evoarch,2,4.1.6-1-ck,195.545358016,compress
9,2015-09-13 15:58:50,evoarch,2,4.1.6-1-ck,197.405528428,compress
10,2015-09-13 16:02:07,evoarch,2,4.1.6-1-ck,207.466166781,compress
1,2015-09-13 16:06:08,evoarch,3,4.1.6-1-ck,9.726959931,make
2,2015-09-13 16:06:31,evoarch,3,4.1.6-1-ck,8.898721229,make
3,2015-09-13 16:06:53,evoarch,3,4.1.6-1-ck,8.569784143,make
4,2015-09-13 16:07:14,evoarch,3,4.1.6-1-ck,9.148697250,make
5,2015-09-13 16:07:36,evoarch,3,4.1.6-1-ck,8.505831670,make
6,2015-09-13 16:07:57,evoarch,3,4.1.6-1-ck,8.312153774,make
7,2015-09-13 16:08:18,evoarch,3,4.1.6-1-ck,9.365910309,make
8,2015-09-13 16:08:40,evoarch,3,4.1.6-1-ck,8.390655740,make
9,2015-09-13 16:09:01,evoarch,3,4.1.6-1-ck,8.571148469,make
10,2015-09-13 16:09:22,evoarch,3,4.1.6-1-ck,9.279834340,make
1,2015-09-13 16:09:35,evoarch,2,4.1.6-1-ck,307.573046098,video
2,2015-09-13 16:14:43,evoarch,2,4.1.6-1-ck,306.853723410,video
3,2015-09-13 16:19:50,evoarch,2,4.1.6-1-ck,308.096435254,video
4,2015-09-13 16:24:58,evoarch,2,4.1.6-1-ck,301.489598407,video
5,2015-09-13 16:30:00,evoarch,2,4.1.6-1-ck,302.470796386,video
6,2015-09-13 16:35:02,evoarch,2,4.1.6-1-ck,294.001217918,video
7,2015-09-13 16:39:56,evoarch,2,4.1.6-1-ck,299.833358862,video
8,2015-09-13 16:44:56,evoarch,2,4.1.6-1-ck,308.043497360,video
9,2015-09-13 16:50:04,evoarch,2,4.1.6-1-ck,296.535504070,video
10,2015-09-13 16:55:01,evoarch,2,4.1.6-1-ck,299.799124974,video

## Video

BENCHMARKs: VC:  71.533s VO:   0.010s A:   0.000s Sys:   2.789s =   74.332s
BENCHMARK%: VC: 96.2339% VO:  0.0138% A:  0.0000% Sys:  3.7523% = 100.0000%

Keydown latency
Tests the delay from keypress to on-screen response.
7.1 frames latency (lower is better)
Scroll latency
Tests the delay from mousewheel movement to on-screen response.
18.8 frames latency (lower is better)
Native reference
Tests the input latency of a native app's window for comparison to the browser.
3.3 frames latency, 5.2 frames jank (lower is better)
Baseline jank
Tests responsiveness while the browser is idle.
CSS: 27.6 frames jank, JavaScript: 27.6 frames jank, Scrolling: 27.6 frames jank (lower is better)
JavaScript jank
Tests responsiveness during JavaScript execution.
CSS: 6.0 frames jank, Scrolling: 132.2 frames jank (lower is better)
Image loading jank
Tests responsiveness during image loading.
CSS: Time_out


➜  latency-benchmark git:(master) ✗ sudo interbench -L 2             
[sudo] password for sergio: 
loops_per_ms unknown; benchmarking...
944953 loops_per_ms saved to file interbench.loops_per_ms
Creating file for read load...

Using 944953 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201509171829

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
Video      0.0 +/- 0.0        0.0                100            100
X          0.0 +/- 0.0        0.0                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.0        0.0                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.0 +/- 0.0        0.0                100            100
Memload    0.0 +/- 0.0        0.4                100            100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
X          0.3 +/- 2.1       16.7                100           98.4
Burn       2.0 +/- 5.8       22.6                100           87.8
Write      0.2 +/- 1.7       19.5                100             99
Read       0.0 +/- 0.1        4.0                100            100
Compile    5.2 +/- 9.3       26.8                100           69.6
Memload    0.3 +/- 2.3       22.7                100           98.2

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       8.4 +/- 17.8      56.0               46.9           37.4
Video     20.2 +/- 35.4     100.0               29.3           20.2
Burn      51.6 +/- 72.0     156.0                 19           8.52
Write     12.5 +/- 24.4      72.0               40.3             31
Read      11.0 +/- 21.2      62.0               42.2           32.2
Compile   56.2 +/- 75.7     189.0               20.3           8.22
Memload   26.2 +/- 43.1     100.0               24.8           15.7

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None      50.4 +/- 51.1      71.9               66.5
Video     86.8 +/- 87.4     111.8               53.5
X         84.3 +/- 86.2     112.9               54.3
Burn     147.0 +/- 148.0    169.3               40.5
Write     64.7 +/- 66.3     112.8               60.7
Read      56.6 +/- 56.9      66.4               63.8
Compile  164.4 +/- 165.6    218.4               37.8
Memload  101.5 +/- 102.0    121.1               49.6


➜  latency-benchmark git:(master) ✗ sudo interbench -L 2 -r
[sudo] password for sergio: 
944953 loops_per_ms read from file interbench.loops_per_ms

Using 944953 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201509171927

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load    Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None       9.0 +/- 9.7       23.0                100            100
Video      7.0 +/- 8.0       16.0                100            100
X          7.0 +/- 8.3       18.0                100            100
Burn       9.0 +/- 10.1      18.0                100            100
Write     18.0 +/- 19.6      30.0                100            100
Read      20.0 +/- 20.4      36.0                100            100
Compile   19.0 +/- 19.6      38.0                100            100
Memload   19.0 +/- 20.3      34.0                100            100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load    Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None       7.0 +/- 8.1       19.0                100            100
X          7.0 +/- 7.8       19.0                100            100
Burn       7.0 +/- 8.1       37.0                100            100
Write     12.0 +/- 13.6      39.0                100            100
Read      14.0 +/- 14.6      30.0                100            100
Compile   14.0 +/- 14.7      40.0                100            100
Memload   17.0 +/- 19.1      88.0                100            100

## Cyclic (without sudo and stress)

T: 0 ( 8005) P:90 I:1000 C:   6316 Min:      5 Act:   57 Avg:   67 Max:    2182
