## Linux Gui - CK 0
--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.1                100            100
Video      0.0 +/- 0.0        0.0                100            100
X          0.0 +/- 0.0        0.1                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.1        1.2                100            100
Read       0.0 +/- 0.0        0.3                100            100
Compile    0.0 +/- 0.0        0.5                100            100
Memload    0.0 +/- 0.1        2.5                100            100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.1                100            100
X          0.0 +/- 0.0        0.2                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.0        0.3                100            100
Read       0.0 +/- 0.0        0.3                100            100
Compile    0.0 +/- 0.0        0.5                100            100
Memload    0.0 +/- 0.0        0.1                100            100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
Video      0.0 +/- 0.0        0.0                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.0        0.0                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.0 +/- 0.0        0.0                100            100
Memload    0.0 +/- 0.0        0.0                100            100

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.0 +/- 0.0        0.0                100
Video      0.0 +/- 0.0        0.0                100
X          0.0 +/- 0.0        0.0                100
Burn       0.0 +/- 0.0        0.0                100
Write      0.0 +/- 0.0        0.0                100
Read       0.0 +/- 0.0        0.0                100
Compile    0.0 +/- 0.0        0.0                100
Memload    0.0 +/- 0.0        0.0                100

## Linux Gui CK 100

[sudo] password for sergio: 
397453 loops_per_ms read from file interbench.loops_per_ms

Using 397453 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ck at datestamp 201509171325

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
Video      0.0 +/- 0.0        0.0                100            100
X          0.0 +/- 0.0        0.0                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.0        0.3                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.0 +/- 0.4       10.0                100            100
Memload    0.0 +/- 0.0        0.0                100            100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.1                100            100
X          0.0 +/- 0.0        0.5                100            100
Burn       0.0 +/- 0.0        0.0                100            100
Write      0.0 +/- 0.0        0.9                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.1 +/- 1.0       16.7                100           99.7
Memload    0.0 +/- 0.1        5.0                100            100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
Video      0.0 +/- 0.0        0.0                100            100
Burn       0.3 +/- 1.5       16.0               94.1           91.7
Write      0.0 +/- 0.0        0.0                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.7 +/- 2.8       18.0                 89           85.2
Memload    0.0 +/- 0.0        0.0                100            100

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.0 +/- 0.0        0.0                100
Video      0.0 +/- 0.0        0.0                100
X          0.0 +/- 0.0        0.0                100
Burn       4.2 +/- 4.8       20.6                 96
Write      0.1 +/- 0.9       13.3               99.9
Read       0.0 +/- 0.0        0.0                100
Compile   11.4 +/- 13.7      41.4               89.7
Memload    0.0 +/- 0.0        0.0                100


## Linux Gui CFS 0

➜  ~  sudo interbench -L 2
[sudo] password for sergio: 
397453 loops_per_ms read from file interbench.loops_per_ms

Using 397453 loops per ms, running every load for 30 sec
Benchmarking kernel 4.1.6-1-ARCH at datestamp 2015091114

--- Benchmarking simulated cpu of Audio in the presence 
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None    ^[[18;5~   0.3 +/- 0.5        8.4               
Video      0.1 +/- 0.1        0.3                100    
X          0.2 +/- 0.2        0.3                100    
Burn       0.0 +/- 0.0        0.0                100    
Write      0.2 +/- 0.3        3.8                100    
Read       0.1 +/- 0.1        0.3                100    
Compile    0.0 +/- 0.4        7.5                100    
Memload    0.3 +/- 1.0        5.0                100    

--- Benchmarking simulated cpu of Video in the presence 
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.3 +/- 0.3        2.4                100    
X          0.2 +/- 0.2        0.3                100    
Burn       0.0 +/- 0.0        0.8                100    
Write      0.2 +/- 0.6       22.1                100    
Read       0.1 +/- 0.1        0.3                100    
Compile    0.0 +/- 0.3        6.1                100    
Memload    0.0 +/- 0.1        1.6                100    

--- Benchmarking simulated cpu of X in the presence of s
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.0 +/- 0.0        0.3                100    
Video      0.0 +/- 0.0        0.2                100    
Burn       0.0 +/- 0.0        0.0                100    
Write      0.0 +/- 0.0        0.2                100    
Read       0.0 +/- 0.0        0.1                100    
Compile    0.0 +/- 0.0        0.0                100    
Memload    0.0 +/- 0.0        0.1                100    

--- Benchmarking simulated cpu of Gaming in the presence
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.0 +/- 0.0        0.0                100
Video      0.0 +/- 0.0        0.0                100
X          0.0 +/- 0.0        0.0                100
Burn       0.0 +/- 0.0        0.0                100
Write      0.0 +/- 0.0        0.0                100
Read       0.0 +/- 0.0        0.0                100
Compile    0.0 +/- 0.3        5.5                100
Memload    0.0 +/- 0.0        0.0                100


