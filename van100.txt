run #,run date,hostname,physical cores/threads,kernel,time(sec),benchmark
1,2015-09-14 15:24:03,evoarch,2,4.1.6-1-ARCH,5.945406474,compress
2,2015-09-14 15:24:09,evoarch,2,4.1.6-1-ARCH,5.916708495,compress
3,2015-09-14 15:24:15,evoarch,2,4.1.6-1-ARCH,6.156781971,compress
4,2015-09-14 15:24:21,evoarch,2,4.1.6-1-ARCH,5.487389552,compress
5,2015-09-14 15:24:27,evoarch,2,4.1.6-1-ARCH,6.190579780,compress
6,2015-09-14 15:24:33,evoarch,2,4.1.6-1-ARCH,5.878318043,compress
7,2015-09-14 15:24:39,evoarch,2,4.1.6-1-ARCH,5.656762194,compress
8,2015-09-14 15:24:45,evoarch,2,4.1.6-1-ARCH,6.288904920,compress
9,2015-09-14 15:24:51,evoarch,2,4.1.6-1-ARCH,5.663226491,compress
10,2015-09-14 15:24:57,evoarch,2,4.1.6-1-ARCH,6.250822163,compress
1,2015-09-14 15:25:28,evoarch,3,4.1.6-1-ARCH,7.836293206,make
2,2015-09-14 15:25:48,evoarch,3,4.1.6-1-ARCH,6.693741670,make
3,2015-09-14 15:26:05,evoarch,3,4.1.6-1-ARCH,6.989339030,make
4,2015-09-14 15:26:22,evoarch,3,4.1.6-1-ARCH,6.071303996,make
5,2015-09-14 15:26:38,evoarch,3,4.1.6-1-ARCH,6.148776257,make
6,2015-09-14 15:26:55,evoarch,3,4.1.6-1-ARCH,6.593382994,make
7,2015-09-14 15:27:11,evoarch,3,4.1.6-1-ARCH,6.901542510,make
8,2015-09-14 15:27:28,evoarch,3,4.1.6-1-ARCH,6.024108740,make
9,2015-09-14 15:27:45,evoarch,3,4.1.6-1-ARCH,6.706090041,make
10,2015-09-14 15:28:00,evoarch,3,4.1.6-1-ARCH,5.982900799,make
1,2015-09-14 15:28:09,evoarch,2,4.1.6-1-ARCH,189.643905402,video
2,2015-09-14 15:31:18,evoarch,2,4.1.6-1-ARCH,178.892830851,video
3,2015-09-14 15:34:17,evoarch,2,4.1.6-1-ARCH,186.191586871,video
4,2015-09-14 15:37:24,evoarch,2,4.1.6-1-ARCH,193.570518214,video
5,2015-09-14 15:40:37,evoarch,2,4.1.6-1-ARCH,209.300437717,video
6,2015-09-14 15:44:07,evoarch,2,4.1.6-1-ARCH,187.901428400,video
7,2015-09-14 15:47:14,evoarch,2,4.1.6-1-ARCH,193.279496159,video
8,2015-09-14 15:50:28,evoarch,2,4.1.6-1-ARCH,194.746791433,video
9,2015-09-14 15:53:43,evoarch,2,4.1.6-1-ARCH,191.395997930,video
10,2015-09-14 15:56:54,evoarch,2,4.1.6-1-ARCH,191.378271025,video

## Internet
Keydown latency
Tests the delay from keypress to on-screen response.
4.6 frames latency (lower is better)
Scroll latency
Tests the delay from mousewheel movement to on-screen response.
8.7 frames latency (lower is better)
Native reference
Tests the input latency of a native app's window for comparison to the browser.
3.2 frames latency, 6.1 frames jank (lower is better)
Baseline jank
Tests responsiveness while the browser is idle.
CSS: 8.9 frames jank, JavaScript: 8.9 frames jank, Scrolling: 9.8 frames jank (lower is better)
JavaScript jank
Tests responsiveness during JavaScript execution.
CSS: 10.2 frames jank, Scrolling: 117.4 frames jank (lower is better)
Image loading jank
Tests responsiveness during image loading.
CSS: 8.3 frames jank, JavaScript: 285.9 frames jank, Scrolling: 285.9 frames jank (lower is better)

## Video

BENCHMARKs: VC:  45.855s VO:   0.006s A:   0.000s Sys:   1.199s =   47.060s
BENCHMARK%: VC: 97.4394% VO:  0.0136% A:  0.0000% Sys:  2.5470% = 100.0000%

## Inter

➜  Videos  sudo interbench -L 2                           
[sudo] password for sergio: 
loops_per_ms unknown; benchmarking...
948144 loops_per_ms saved to file interbench.loops_per_ms
Creating file for read load...

Using 948144 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201509172002

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
Video      0.0 +/- 0.0        0.0                100            100
X          0.7 +/- 1.9       10.0                100            100
Burn       2.0 +/- 3.4        5.6                100            100
Write      0.1 +/- 0.8       11.9                100            100
Read       0.0 +/- 0.0        0.0                100            100
Compile    0.1 +/- 0.7        5.7                100            100
Memload    0.0 +/- 0.0        0.5                100            100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       0.0 +/- 0.0        0.0                100            100
X          0.4 +/- 2.7       34.1               99.9           98.4
Burn       6.7 +/- 11.4      49.3               99.1           64.5
Write      0.6 +/- 3.3       48.1               99.9           97.4
Read       0.0 +/- 0.1        1.1                100            100
Compile    7.9 +/- 12.3      48.3               98.1           57.6
Memload    0.0 +/- 0.1        1.6                100            100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None       7.0 +/- 16.5      66.0               54.5           45.2
Video     20.2 +/- 35.9     104.0               28.3           19.4
Burn      63.5 +/- 87.8     200.0               17.4           7.18
Write     12.3 +/- 25.4     119.0               41.3             32
Read       7.9 +/- 17.5      80.0               53.8           43.1
Compile   75.5 +/- 102.6    230.0               15.2           5.75
Memload   24.9 +/- 41.7     105.0               25.2           16.1

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load    Latency +/- SD (ms)  Max Latency   % Desired CPU
None       0.9 +/- 4.1       56.8               99.1
Video     67.0 +/- 67.2      76.6               59.9
X         62.2 +/- 71.3     113.1               61.6
Burn     203.1 +/- 206.2    265.5                 33
Write     21.3 +/- 28.8     111.0               82.4
Read       9.1 +/- 10.1      67.4               91.7
Compile  249.7 +/- 254.0    355.9               28.6
Memload   96.6 +/- 97.2     105.7               50.9


➜  Videos  sudo interbench -L 2 -r
[sudo] password for sergio: 
948144 loops_per_ms read from file interbench.loops_per_ms

Using 948144 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201509172025

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load    Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None      11.0 +/- 12.2      23.0                100            100
Video     12.0 +/- 12.3      18.0                100            100
X         12.0 +/- 13.7     103.0                100            100
Burn      11.0 +/- 11.6      17.0                100            100
Write     23.0 +/- 65.6    1195.0                100            100
Read      15.0 +/- 15.8      34.0                100            100
Compile   21.0 +/- 21.6      37.0                100            100
Memload   25.0 +/- 25.9      43.0                100            100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load    Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None      14.0 +/- 59.7    2286.0                100            100
X         12.0 +/- 14.8     113.0                100            100
Burn      11.0 +/- 12.0      21.0                100            100
Write     18.0 +/- 36.8     746.0                100            100
Read      28.0 +/- 191.8   6282.0                100            100
Compile   18.0 +/- 22.1     348.0                100            100
Memload   27.0 +/- 82.7    3210.0                100            100

