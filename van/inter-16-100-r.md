
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201601131343

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	  14.0 +/- 14.6      31.0		 100	        100
Video	  13.0 +/- 14.2      23.0		 100	        100
X	  13.0 +/- 14.3      30.0		 100	        100
Burn	  13.0 +/- 13.6      28.0		 100	        100
Write	  23.0 +/- 39.1     526.0		 100	        100
Read	  19.0 +/- 20.0      59.0		 100	        100
Compile	  24.0 +/- 30.5     448.0		 100	        100
Memload	  26.0 +/- 26.7      55.0		 100	        100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	  14.0 +/- 15.2      46.0		 100	        100
X	  13.0 +/- 14.9     123.0		 100	        100
Burn	  12.0 +/- 12.8      30.0		 100	        100
Write	  21.0 +/- 40.8     772.0		 100	        100
Read	  20.0 +/- 21.0      68.0		 100	        100
Compile	  21.0 +/- 27.8     435.0		 100	        100
Memload	  25.0 +/- 26.1      86.0		 100	        100


