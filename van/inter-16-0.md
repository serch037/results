
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201601160612

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.3 +/- 0.3        0.7		 100	        100
Video	   0.1 +/- 0.1        0.1		 100	        100
X	   0.1 +/- 0.2        0.3		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.2 +/- 0.2        1.9		 100	        100
Read	   0.1 +/- 0.1        0.3		 100	        100
Compile	   0.0 +/- 0.4        7.7		 100	        100
Memload	   0.0 +/- 0.0        0.2		 100	        100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.3 +/- 0.3        0.3		 100	        100
X	   0.2 +/- 0.2        0.3		 100	        100
Burn	   0.0 +/- 0.8       16.7		 100	       99.8
Write	   0.2 +/- 1.0       20.3		 100	       99.7
Read	   0.1 +/- 0.1        0.3		 100	        100
Compile	   0.3 +/- 2.1       21.1		 100	       98.5
Memload	   0.0 +/- 0.1        1.3		 100	        100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.2 +/- 1.0        6.0		93.5	       91.1
Video	   0.8 +/- 2.8       13.0		84.5	       79.1
Burn	  26.8 +/- 43.7     114.0		26.8	       16.7
Write	   0.9 +/- 3.1       18.0		83.8	       79.2
Read	   1.1 +/- 3.4       16.0		82.3	       76.4
Compile	  26.6 +/- 46.0     130.0		25.8	       16.5
Memload	   1.1 +/- 3.4       14.0		82.5	       77.1

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU
None	   6.6 +/- 7.4       14.0		93.8
Video	   9.4 +/- 9.4       11.0		91.4
X	   9.3 +/- 9.3       13.4		91.5
Burn	  64.4 +/- 73.7     126.3		60.8
Write	  10.0 +/- 10.0      16.7		90.9
Read	   9.2 +/- 9.3       24.4		91.6
Compile	  74.4 +/- 84.9     165.1		57.3
Memload	  10.4 +/- 12.0      71.8		90.6


