
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201601160633

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.0		 100	        100
Video	   0.0 +/- 0.0        0.0		 100	        100
X	   0.6 +/- 1.7        6.6		 100	        100
Burn	   4.2 +/- 5.9       10.0		 100	        100
Write	   0.1 +/- 0.5        9.8		 100	        100
Read	   0.0 +/- 0.0        0.0		 100	        100
Compile	   1.0 +/- 2.7       11.5		 100	        100
Memload	   0.0 +/- 0.3        6.4		 100	        100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.0		 100	        100
X	   0.5 +/- 3.1       39.8		99.9	       97.6
Burn	   8.5 +/- 13.2      46.8		96.8	       55.7
Write	   0.8 +/- 3.8       38.5		99.7	       96.5
Read	   0.0 +/- 0.0        0.2		 100	        100
Compile	   9.9 +/- 13.9      53.6		96.6	       45.4
Memload	   0.3 +/- 2.4       29.7		 100	       98.6

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   9.9 +/- 20.7      96.0		49.5	         38
Video	  31.4 +/- 49.5     112.0		22.1	         13
Burn	  80.4 +/- 112.5    280.0		14.6	       5.84
Write	  14.0 +/- 29.7     108.0		40.1	       30.7
Read	  10.2 +/- 22.5      99.0		47.1	       37.5
Compile	  85.4 +/- 117.0    286.0		  13	       4.57
Memload	  33.2 +/- 52.1     124.0		20.7	       11.9

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU
None	   9.3 +/- 9.6       46.3		91.5
Video	  95.1 +/- 95.5      97.7		51.3
X	  77.6 +/- 86.7     130.5		56.3
Burn	 230.6 +/- 234.3    306.3		30.2
Write	  30.1 +/- 42.6     150.8		76.9
Read	  17.9 +/- 18.0      32.6		84.8
Compile	 280.9 +/- 286.2    393.4		26.3
Memload	 120.9 +/- 121.5    142.2		45.3


