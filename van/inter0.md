
Using 399199 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201511290922

--- Benchmarking simulated cpu of Audio in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.2 +/- 0.2        0.2		 100	        100
Video	   0.2 +/- 0.2        0.2		 100	        100
X	   0.2 +/- 0.2        0.3		 100	        100
Burn	   0.0 +/- 0.0        0.2		 100	        100
Write	   0.2 +/- 0.2        1.8		 100	        100
Read	   0.1 +/- 0.1        0.3		 100	        100
Compile	   0.0 +/- 0.1        2.8		 100	        100
Memload	   0.0 +/- 0.0        0.1		 100	        100

--- Benchmarking simulated cpu of Video in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.3 +/- 0.3        0.3		 100	        100
X	   0.2 +/- 0.2        0.3		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.2 +/- 0.2        4.0		 100	        100
Read	   0.1 +/- 0.1        0.3		 100	        100
Compile	   0.0 +/- 0.3        9.3		 100	        100
Memload	   0.0 +/- 0.1        1.2		 100	        100

--- Benchmarking simulated cpu of X in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU  % Deadlines Met
None	   0.0 +/- 0.0        0.2		 100	        100
Video	   0.0 +/- 0.0        0.1		 100	        100
Burn	   0.0 +/- 0.0        0.0		 100	        100
Write	   0.0 +/- 0.0        0.2		 100	        100
Read	   0.0 +/- 0.0        0.1		 100	        100
Compile	   0.0 +/- 0.0        0.0		 100	        100
Memload	   0.0 +/- 0.0        0.0		 100	        100

--- Benchmarking simulated cpu of Gaming in the presence of simulated ---
Load	Latency +/- SD (ms)  Max Latency   % Desired CPU
None	   0.0 +/- 0.0        0.0		 100
Video	   0.0 +/- 0.0        0.0		 100
X	   0.0 +/- 0.0        0.0		 100
Burn	   0.0 +/- 0.0        0.0		 100
Write	   0.0 +/- 0.0        0.0		 100
Read	   0.0 +/- 0.0        0.0		 100
Compile	   0.0 +/- 0.5        9.4		 100
Memload	   0.0 +/- 0.0        0.0		 100


