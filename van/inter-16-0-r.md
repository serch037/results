
Using 1038282 loops per ms, running every load for 30 seconds
Benchmarking kernel 4.1.6-1-ARCH at datestamp 201601131332

--- Benchmarking simulated cpu of Audio real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	 242.0 +/- 244.0    262.0		 100	        100
Video	 130.0 +/- 131.4    146.0		 100	        100
X	 161.0 +/- 174.5    318.0		 100	        100
Burn	  11.0 +/- 11.1      17.0		 100	        100
Write	 160.0 +/- 183.3    922.0		 100	        100
Read	 105.0 +/- 111.3    281.0		 100	        100
Compile	  20.0 +/- 21.1     104.0		 100	        100
Memload	 153.0 +/- 154.4    189.0		 100	        100

--- Benchmarking simulated cpu of Video real time in the presence of simulated ---
Load	Latency +/- SD (us)  Max Latency   % Desired CPU  % Deadlines Met
None	 305.0 +/- 1080.5  16919.0		 100	       99.6
X	 408.0 +/- 2081.6  16919.0		 100	       98.5
Burn	  12.0 +/- 12.4      20.0		 100	        100
Write	 530.0 +/- 2491.3  18847.0		 100	       97.8
Read	 156.0 +/- 974.4   16778.0		 100	       99.7
Compile	  18.0 +/- 20.5     245.0		 100	        100
Memload	 198.0 +/- 1058.7  16830.0		 100	       99.6


